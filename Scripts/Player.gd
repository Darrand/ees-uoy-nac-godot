extends KinematicBody2D

var motion = Vector2()
export (int) var ACCELERATION = 500
export (int) var DECELERATION = 500
export (int) var speed = 2000

var lights = false
var current_light
onready var spawn_pos = get_node("../SpawnPoints/PlayerSpawn")

# Called when the node enters the scene tree for the first time.
func _ready():
	#Hide all lights
	lights_off()
	#Reset the player
	reset()

func reset():
	# reset player position
	print("player reset")
	self.set_position(spawn_pos.get_position())
	
func get_input_axis():
	look_at(get_global_mouse_position())
	var axis = Vector2()
	axis.x = int(Input.is_action_pressed("move_right")) - int(Input.is_action_pressed("move_left"))
	axis.y = int(Input.is_action_pressed("move_down")) - int(Input.is_action_pressed("move_up"))
	return axis.normalized()
	
func get_input_light():
	if Input.is_action_just_pressed("left_click"):
		var sound = AudioStreamPlayer.new()
		self.add_child(sound)
		sound.stream = load("res://Asset/light-switch-pull-chain-daniel_simon.wav")
		sound.play()
		if !lights:
			lights = true
			lights_on()
		else:
			lights = false
			lights_off()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	# Light prompt
	get_input_light()
	# Movement
	var axis = get_input_axis()
	if axis == Vector2.ZERO:
		friction(DECELERATION * delta)
	else:
		movement(ACCELERATION * axis * delta)
	motion = move_and_slide(motion)
	
func friction(amount):
	if motion.length() > amount:
		motion -= motion.normalized() * amount
	else:
		motion = Vector2.ZERO
	
func movement(acceleration):
	motion += acceleration
	motion.clamped(speed)
	
func lights_on():
	if global.lives == 3:
		$Lights/Light2D_s.show()
		$Lights/Light2D_s/LightCollider/CollisionShape2D.disabled = false
	elif global.lives == 2:
		$Lights/Light2D_m.show()
		$Lights/Light2D_m/LightCollider/CollisionShape2D.disabled = false
	elif global.lives == 1:
		$Lights/Light2D_l.show()
		$Lights/Light2D_l/LightCollider/CollisionShape2D.disabled = false

func lights_off():
	$Lights/Light2D_s.hide()
	$Lights/Light2D_m.hide()
	$Lights/Light2D_l.hide()
	$Lights/Light2D_s/LightCollider/CollisionShape2D.disabled = true
	$Lights/Light2D_m/LightCollider/CollisionShape2D.disabled = true
	$Lights/Light2D_l/LightCollider/CollisionShape2D.disabled = true