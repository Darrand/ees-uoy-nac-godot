extends Area2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_EnemyCollider_body_entered(body):
	if body.get_name() == "Monster": 
		body.chomp()
		if global.lives > 0:
			global.lives -= 1
			get_parent().reset()
			body.reset()
		if global.lives == 0:
			get_tree().change_scene(str("res://Scenes/GameOver.tscn"))