extends StaticBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var sp5 = get_node("../SpawnPoints/SoulSpawn5")
onready var sp6 = get_node("../SpawnPoints/SoulSpawn6")
# Called when the node enters the scene tree for the first time.
func _ready():
	spawn()

func spawn():
	var spawn_pos = [sp5, sp6]
	set_position(spawn_pos[randi() % spawn_pos.size()].get_position())

func picked():
	hide()
	$CollisionShape2D.disabled = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
