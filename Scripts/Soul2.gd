extends StaticBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var sp3 = get_node("../SpawnPoints/SoulSpawn3")
onready var sp4 = get_node("../SpawnPoints/SoulSpawn4")
# Called when the node enters the scene tree for the first time.
func _ready():
	spawn()

func spawn():
	var spawn_pos = [sp3, sp4]
	set_position(spawn_pos[randi() % spawn_pos.size()].get_position())

func picked():
	hide()
	$CollisionShape2D.disabled = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
