extends StaticBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var sp = get_node("../SpawnPoints/SoulSpawn")
onready var sp2 = get_node("../SpawnPoints/SoulSpawn2")
# Called when the node enters the scene tree for the first time.
func _ready():
	spawn()

func spawn():
	var spawn_pos = [sp, sp2]
	set_position(spawn_pos[randi() % spawn_pos.size()].get_position())

func picked():
	hide()
	$CollisionShape2D.disabled = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
