extends Area2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	$locked.show()
	$Open.hide()

func _process(delta):
	if global.souls == 3:
		$locked.hide()
		$Open.show()

func _on_WinDoor_body_entered(body):
	if body.get_name() == "Player":
		if global.souls == 3:
			get_tree().change_scene(str("res://Scenes/Win.tscn"))