extends KinematicBody2D

var chase_speed = 10000
var away_speed = 5000
var chasing = true
var velocity = Vector2()
onready var ye_boi = get_node("../Player")
onready var sp1 = get_node("../SpawnPoints/SpawnPoint")
onready var sp2 = get_node("../SpawnPoints/SpawnPoint2")
onready var sp3 = get_node("../SpawnPoints/SpawnPoint3")
onready var sp4 = get_node("../SpawnPoints/SpawnPoint4")

# Called when the node enters the scene tree for the first time.
func _ready():
	chasing = true
	$Chase.play()
	hide()
	reset()
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	chase_player()
	velocity = move_and_slide(velocity * delta)

func chomp():
	$Chomp.play()

func reset():
	# respawn
	random_spawn()
	# change speed
	if global.lives == 3:
		chase_speed = 10000
		away_speed = 5000
	elif global.lives == 2:
		chase_speed = 12500
		away_speed = 7000
	elif global.lives == 1:
		chase_speed = 15000
		away_speed = 9000

func random_spawn():
	var spawn_point = [sp1, sp2, sp3, sp4]
	var spawn_pos = spawn_point[randi() % spawn_point.size()].get_position()
	set_position(spawn_pos)

func chase_player():
	if chasing:
		if ye_boi.get_position().x > get_position().x:
			velocity.x += chase_speed
		if ye_boi.get_position().x < get_position().x:
			velocity.x -= chase_speed
		if ye_boi.get_position().y > get_position().y:
			velocity.y += chase_speed
		if ye_boi.get_position().y < get_position().y:
			velocity.y -= chase_speed
	else:
		if ye_boi.get_position().x > get_position().x:
			velocity.x -= away_speed
		if ye_boi.get_position().x < get_position().x:
			velocity.x += away_speed
		if ye_boi.get_position().y > get_position().y:
			velocity.y -= away_speed
		if ye_boi.get_position().y < get_position().y:
			velocity.y += away_speed