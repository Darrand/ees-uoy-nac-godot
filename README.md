# eeS-uoY-naC-Godot

Godot Game Development Project

Created for CSUI 2019/2020 Game Development Course

## Asset Credits
- [freesoundeffects.com](https://www.freesoundeffects.com/)
- [Godot Engine demo Project](https://github.com/godotengine/godot-demo-projects)
- [1001fonts.com](1001fonts.com)
- [incompetech.com](incompetech.com)
- [soundbible.com](soundbible.com)
